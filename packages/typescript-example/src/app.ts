import Product from 'models/product';
import Cart from 'models/cart';
import Shop from 'models/shop';

const milk = new Product('milk');
milk.setPrice({
  value: 13,
  currency: 'PLN'
});

const bread = new Product('bread');
const butter = new Product('butter');

const cart = new Cart();
cart.add(milk);
cart.addMany([bread, butter])
const list = cart.getList();
console.log('Current cart list:', list);

const messageEl = document.getElementById('message');
messageEl.innerText = list.join(',');

cart.removeAll();
console.log('Removed! List:', cart.getList());

Shop.placeOrder(cart);



