import type {PriceType} from 'types/price';

class Product {
  name: string = 'product';
  price: PriceType;

  constructor(name: string) {
       this.name = name;
  }

  setPrice(price: PriceType) {
    this.price = price;
  }
}

export default Product;