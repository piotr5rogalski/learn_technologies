import Product from 'models/product';

class Cart {
  items: Product[] = [];

  constructor(items?: Product[]) {
    this.items = items ?? [];
    console.log(this.items)
  }

  add(product: Product) {
    this.items.push(product);
  }

  addMany(products: Product[]) {
    products.forEach(product => this.add(product));
  }

  getList() {
    return this.items.map(item => item.name);
  }

  removeAll(): boolean {
    this.items = [];
    return true;
  }
}

export default Cart;