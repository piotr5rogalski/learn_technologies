import Cart from 'models/cart';

class Shop {
  static placeOrder(cart: Cart) {
    console.log('Placing order:', {cart});
  }
}
export default Shop;